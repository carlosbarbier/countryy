require_relative 'lib/countryy/version'

Gem::Specification.new do |spec|
  spec.name          = "countryy"
  spec.version       = Countryy::VERSION
  spec.authors       = ["carlos barbier"]
  spec.email         = ["carlos.barbier@outlook.com"]

  spec.summary       = %q{A gem to generate information about all the countries}
  spec.homepage      = "https://gitlab.com/carlosbarbier/countryy"
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.3.0")
  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/carlosbarbier/countryy"

  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
end
